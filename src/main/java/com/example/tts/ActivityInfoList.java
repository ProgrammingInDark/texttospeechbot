
package com.example.tts;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "activityName",
    "activityIcon",
    "objectiveInfoList"
})
public class ActivityInfoList {

    @JsonProperty("activityName")
    private String activityName;
    @JsonProperty("activityIcon")
    private String activityIcon;
    @JsonProperty("objectiveInfoList")
    private List<ObjectiveInfoList> objectiveInfoList = null;

    @JsonProperty("activityName")
    public String getActivityName() {
        return activityName;
    }

    @JsonProperty("activityName")
    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    @JsonProperty("activityIcon")
    public String getActivityIcon() {
        return activityIcon;
    }

    @JsonProperty("activityIcon")
    public void setActivityIcon(String activityIcon) {
        this.activityIcon = activityIcon;
    }

    @JsonProperty("objectiveInfoList")
    public List<ObjectiveInfoList> getObjectiveInfoList() {
        return objectiveInfoList;
    }

    @JsonProperty("objectiveInfoList")
    public void setObjectiveInfoList(List<ObjectiveInfoList> objectiveInfoList) {
        this.objectiveInfoList = objectiveInfoList;
    }

}
