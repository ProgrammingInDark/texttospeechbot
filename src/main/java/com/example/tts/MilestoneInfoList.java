
package com.example.tts;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "milestoneName",
    "milestoneIcon",
    "activityInfoList"
})
public class MilestoneInfoList {

    @JsonProperty("milestoneName")
    private String milestoneName;
    @JsonProperty("milestoneIcon")
    private String milestoneIcon;
    @JsonProperty("activityInfoList")
    private List<ActivityInfoList> activityInfoList = null;

    @JsonProperty("milestoneName")
    public String getMilestoneName() {
        return milestoneName;
    }

    @JsonProperty("milestoneName")
    public void setMilestoneName(String milestoneName) {
        this.milestoneName = milestoneName;
    }

    @JsonProperty("milestoneIcon")
    public String getMilestoneIcon() {
        return milestoneIcon;
    }

    @JsonProperty("milestoneIcon")
    public void setMilestoneIcon(String milestoneIcon) {
        this.milestoneIcon = milestoneIcon;
    }

    @JsonProperty("activityInfoList")
    public List<ActivityInfoList> getActivityInfoList() {
        return activityInfoList;
    }

    @JsonProperty("activityInfoList")
    public void setActivityInfoList(List<ActivityInfoList> activityInfoList) {
        this.activityInfoList = activityInfoList;
    }

}
