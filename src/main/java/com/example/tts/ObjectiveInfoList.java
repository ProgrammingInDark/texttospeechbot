
package com.example.tts;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "objectiveDescription",
    "objectiveCompletionValue"
})
public class ObjectiveInfoList {

    @JsonProperty("objectiveDescription")
    private String objectiveDescription;
    @JsonProperty("objectiveCompletionValue")
    private Integer objectiveCompletionValue;

    @JsonProperty("objectiveDescription")
    public String getObjectiveDescription() {
        return objectiveDescription;
    }

    @JsonProperty("objectiveDescription")
    public void setObjectiveDescription(String objectiveDescription) {
        this.objectiveDescription = objectiveDescription;
    }

    @JsonProperty("objectiveCompletionValue")
    public Integer getObjectiveCompletionValue() {
        return objectiveCompletionValue;
    }

    @JsonProperty("objectiveCompletionValue")
    public void setObjectiveCompletionValue(Integer objectiveCompletionValue) {
        this.objectiveCompletionValue = objectiveCompletionValue;
    }

}
