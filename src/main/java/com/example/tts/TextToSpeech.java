package com.example.tts;

import marytts.LocalMaryInterface;
import marytts.exceptions.MaryConfigurationException;
import marytts.exceptions.SynthesisException;

import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class TextToSpeech {

    public void textToSpeech(String text) throws IOException, SynthesisException,
            MaryConfigurationException {

        LocalMaryInterface mary = null;
        mary = new LocalMaryInterface();

        AudioInputStream audio = null;

        mary.setLocale(Locale.US);
        //mary.setVoice("cmu-rms-hsmm");
        audio = mary.generateAudio(text);

        AudioSystem.write(audio, AudioFileFormat.Type.WAVE, new File("test.wav"));
    }

    public void playFile(String filename) throws LineUnavailableException, IOException, UnsupportedAudioFileException, InterruptedException {

            Clip clip = AudioSystem.getClip();
            clip.open(AudioSystem.getAudioInputStream(new File(filename)));
            clip.start();

            Thread.sleep(TimeUnit.MICROSECONDS.toMillis(clip.getMicrosecondLength()));

            clip.stop();

    }
}
