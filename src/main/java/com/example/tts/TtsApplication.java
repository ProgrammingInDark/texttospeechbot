package com.example.tts;

import marytts.LocalMaryInterface;
import marytts.exceptions.MaryConfigurationException;
import marytts.exceptions.SynthesisException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.client.RestTemplate;

import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Locale;

@SpringBootApplication
public class TtsApplication {

    public static void main(String[] args) throws UnsupportedAudioFileException, IOException, LineUnavailableException, MaryConfigurationException, SynthesisException, InterruptedException {
        SpringApplication.run(TtsApplication.class, args);

        TextToSpeech speech = new TextToSpeech();

        RestTemplate template = new RestTemplate();

        MilestoneInfoList[] milestones = template.getForObject("http://ueclan.herokuapp.com/api/destiny/milestones",
                MilestoneInfoList[].class);

        String today = "Tuesday";

        if(today.equals("Tuesday")){
            String milestoneAnnounce ="Hello, Guardian. Today is Tuesday. Let me bring up this week's milestones";
            speech.textToSpeech(milestoneAnnounce);
            speech.playFile("test.wav");
        }
        Thread.sleep(500);

        String milestoneTest ="This is this week's milestones. ";
        speech.textToSpeech(milestoneTest);
        speech.playFile("test.wav");

        Thread.sleep(500);

        String text = "";

        for (int i = 0; i < milestones.length; i++) {

            text = "";
            if(milestones[i].getMilestoneName().equals("Classified")){
                text += "This milestone is classified. ";
                text.concat("\n");
            }
            else{
                text += milestones[i].getMilestoneName()+". ";
                text.concat("\n");
            }

            if (milestones[i].getActivityInfoList() != null) {

                for (int j = 0; j < milestones[i].getActivityInfoList().size(); j++) {

                    text += "Activity: "+milestones[i].getActivityInfoList().get(j).getActivityName()+". ";
                    text.concat("\n");

                    if (milestones[i].getActivityInfoList().get(j).getObjectiveInfoList() != null) {

                        for (int k = 0; k < milestones[i].getActivityInfoList().get(j).getObjectiveInfoList().size();
                             k++) {

                            text +="Objective: "+milestones[i].getActivityInfoList().get(j).getObjectiveInfoList().get(k)
                                    .getObjectiveDescription()+". ";
                            text.concat("\n");
                        }
                    }
                }
            }
            speech.textToSpeech(text);
            speech.playFile("test.wav");
            Thread.sleep(500);
        }


    }
}
